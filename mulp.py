#!/usr/bin/python
# vim:set et sw=4 ts=4 sts=0:
from __future__ import nested_scopes

import ast
import re
import sys

defaults = {
    'prefix': '(\s*)###@',
    'trim': '(\s*)### ',
}
regions = {}

def _getstring(text):
    """
    Extracts a leading Python string literal (shortstring without prefix) from
    a string. Returns a tuple consisting of the parsed string literal and the
    remaining text.
    """
    if len(text) < 2:
        raise ValueError("Missing or incomplete attribute value (too short)")
    quote = text[0]
    base = 1
    while True:
        pos = text.find(quote, base)
        if pos == -1:
            raise ValueError("Attribute value is missing closing quote")
        try:
            value = ast.literal_eval(text[0:pos+1])
        except:
            base = pos+1
            continue
        return value, text[pos+1:]

def _parse_params(text):
    """
    Parses a sequence of attributes much like those in HTML tags. Assumes that
    the attribute names consist of alphanumeric (\w) characters and terminates
    when something that is not a valid name is encountered, or at the end of
    the input.
    Returns a dict of attributes and the remaining text from the input.
    """
    params = {}
    while True:
        param, sep, text = text.partition('=')
        if not sep:
            text = param
            break
        if not re.match(r'^\w+$', param.strip()):
            text = param + sep + text
            break
        param = param.strip()
        try:
            value, text = _getstring(text)
        except ValueError as e:
            raise ValueError("Parameter '%s':" % param, *e.args)
        params[param] = value
    return params, text

def _tag_defaults(params):
    defaults.update(params)

def _process_render(params, content):
    lineno = 0
    lineno_cheat = 0
    isdoc = False
    isexec = False
    outdent = 0
    indent = 0
    pre = ''
    post = ''
    captures = []
    captures_outdent = []
    fn = params['file']
    regions[fn] = []
    doc = []
    re_prefix = re.compile(params['prefix'])
    re_trim = re.compile('^' + params['trim'])
    re_name = re.compile(r'(/?\w+)\s*')
    re_wsonly = re.compile(r'\s*$')
    prevstate = {
        'doc': False,
        'code': False,
    }

    def startcode():
        if prevstate['doc']:
            prevstate['doc'] = False
            doc.append(pre)
        prevstate['code'] = True

    def startdoc():
        if prevstate['code']:
            prevstate['code'] = False
            doc.append(post)
        prevstate['doc'] = True

    while len(content) > 0:
        line = content.pop(0)
        if lineno_cheat:
            lineno_cheat -= 1
        else:
            lineno += 1
        err = (params['file'], lineno)
        prefix_match = re_prefix.match(line)
        if prefix_match:
            m = re_name.match(line, prefix_match.end())
            if not m:
                raise ValueError("File '%s': line %i: found command prefix but no command" % err)
            name = m.group(1).lower()
            cparams, rest = _parse_params(line[m.end():])
            if name == 'doc':
                isdoc = True
                if prefix_match.lastindex:
                    outdent = prefix_match.group(1)
                else:
                    outdent = ''
                startdoc()
                indent = cparams.get('indent', params.get('indent', ''))
                pre = cparams.get('pre', params.get('pre', ''))
                post = cparams.get('post', params.get('post', ''))
            elif name == '/doc':
                if not isdoc:
                    raise ValueError("File '%s': line %i: stray '/doc'" % err)
                if isexec:
                    raise ValueError("File '%s': line %i: '/doc' but an 'exec' block is still open" % err)
                startdoc()  # terminate code block, if any
                isdoc = False
            elif name == 'grab':
                grabname = cparams['name']
                if grabname in captures:
                    raise ValueError("File '%s': line %i: grab region '%s' is already active" % (err + (grabname,)))
                captures.append(grabname)
                regions.setdefault(grabname, [])
                if prefix_match.lastindex:
                    captures_outdent.append(prefix_match.group(1))
                else:
                    captures_outdent.append('')
            elif name == '/grab':
                if not len(captures):
                    raise ValueError("File '%s': line %i: stray '/grab'" % err)
                captures.pop()
                captures_outdent.pop()
            elif name == 'include':
                if not isdoc:
                    raise ValueError("File '%s': line %i: include can only be used in doc blocks" % err)
                incname = cparams['name']
                if not incname in regions:
                    raise ValueError("File '%s': line %i: attempt to include unknown grab region '%s'" % (err + (incname,)))
                startcode()
                content[0:0] = [outdent + r for r in regions[incname]]
                lineno_cheat += len(regions[incname])
            elif name == 'print':
                if not isdoc:
                    raise ValueError("File '%s': line %i: print can only be used in doc blocks" % err)
                startdoc()
                doc.append(0, eval(cparams['expr']))
            elif name == 'exec':
                if not isdoc:
                    raise ValueError("File '%s': line %i: exec can only be used in doc blocks" % err)
                startdoc()
                isexec = True
                exec_buffer = ''
            elif name == '/exec':
                if not isexec:
                    raise ValueError("File '%s': line %i: stray '/exec'" % err)
                cc = compile(exec_buffer, "<%s exec block ending at line %i>" % err, 'exec')
                eval(cc, {
                    'output': (lambda x: doc.append(x))
                })
                isexec = False
            continue

        if isdoc:
            (line, trimmed) = re_trim.subn('', line, count=1)
            if trimmed:
                if isexec:
                    exec_buffer += line
                else:
                    # doc line
                    startdoc()
                    doc.append(line)
                continue
            else:
                # code line
                cline = re.sub('^' + re.escape(outdent), '', line, count=1)
                startcode()
                doc.append(indent + cline)

        for c, coutdent in zip(captures, captures_outdent):
            cline = re.sub('^' + re.escape(coutdent), '', line)
            regions[c].append(cline)
        regions[fn].append(line)
    return doc

def _tag_render(params):
    merged_params = dict(defaults)
    merged_params.update(params)
    params = merged_params
    with open(params['file']) as f:
        content = f.readlines()
        doc = _process_render(params, content)
        print(''.join(doc))

def _tag_open(params):
    print('<%')

def _tag_close(params):
    print('%>')

if len(sys.argv) > 1:
    f = open(sys.argv[1])
else:
    f = sys.stdin
text = f.read()
f.close()

while True:
    # Open tag
    m = re.search(r'<%\s*', text)
    if not m:
        # That's all, folks!
        print(text)
        break
    print(text[:m.start()])
    text = text[m.end():]
    # Tag name
    m = re.match(r'(\w+)\s*', text)
    if not m:
        raise ValueError("Open tag without name")
    tagname = m.group(1)
    text = text[m.end():]
    params, text = _parse_params(text)
    # Tag end
    m = re.match(r'\s*%>', text)
    if not m:
        raise ValueError("Unclosed tag")
    text = text[m.end():]

    func = globals()['_tag_%s' % tagname]
    if not func:
        raise ValueError("Unknown tag '%s'" % tagname)
    func(params)
